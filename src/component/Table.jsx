import React, { Component } from "react";
import { Table, Button, Form } from "react-bootstrap";

export default class TableComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tblData: props.data,
      selected: [],
    };
    this.data = props.data;
    this.selectEvent = this.selectEvent.bind(this);
    this.deleteSelected = this.deleteSelected.bind(this);
    this.getSelectedRow = this.getSelectedRow.bind(this);
  }
  selectEvent(idx) {
    let selectedRow = this.state.selected;
    let isFound = false;
    if (selectedRow.length === 0) {
      selectedRow.push(idx);
    } else {
      selectedRow.map((item, index) => {
        if (item === idx) {
          selectedRow.splice(index, 1);
          isFound = true;
        }
      });
      if (!isFound) {
        selectedRow.push(idx);
      }
    }

    selectedRow.sort();
    this.setState({ selected: selectedRow });
  }
  deleteSelected = () => {
    let selectedRow = this.state.selected;
    let selectedRowTemp = [];
    let count = 0;
    let tempData = this.state.tblData;
    selectedRow.map((item) => {
      tempData.splice(item - count, 1);
      count++;
      selectedRowTemp.push(item);
    });
    selectedRowTemp.map((item) => {
      selectedRow.splice(selectedRow.indexOf(item), 1);
      count++;
    });
    this.setState({
      tblData: tempData,
      selected: selectedRow,
    });
  };
  getSelectedRow = (idx) => {
    let selectedRow = this.state.selected;
    if (selectedRow.indexOf(idx) !== -1) {
      return true;
    }
    return false;
  };
  render() {
    return (
      <div>
        <h1>Table Account</h1>
        <Table striped bordered hover variant="dark">
          <thead>
            <tr style={{ textAlign: "center" }}>
              <th>#</th>
              <th>USERNAME</th>
              <th>EMAIL</th>
              <th>GENDER</th>
              <th>SELECT</th>
            </tr>
          </thead>
          <tbody>
            {this.state.tblData.map((item, index) => (
              <tr
                key={index}
                onClick={() => this.selectEvent(index)}
                style={{ textAlign: "center" }}
              >
                <td>{index + 1}</td>
                <td>{item.username}</td>
                <td>{item.email}</td>
                <td>{item.gender}</td>
                <td>
                  {this.getSelectedRow(index) ? (
                    <Form>
                      <Form.Check custom inline checked={true} readOnly />
                    </Form>
                  ) : (
                    <Form>
                      <Form.Check custom inline checked={false} readOnly />
                    </Form>
                  )}
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
        <Button variant="danger" type="button" onClick={this.deleteSelected}>
          Delete
        </Button>
      </div>
    );
  }
}
