import { Container, Navbar } from "react-bootstrap";
import React, { Component } from "react";

export default class NavigationBar extends Component {
  render() {
    return (
      <Navbar>
        <Container>
          <Navbar.Brand href="#home">KSHRD Student</Navbar.Brand>
          <Navbar.Toggle />
          <Navbar.Collapse className="justify-content-end">
            <Navbar.Text>
              Signed in as: <a href="#login">Voth</a>
            </Navbar.Text>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    );
  }
}
