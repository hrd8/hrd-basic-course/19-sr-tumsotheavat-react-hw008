import NavigationBar from "./component/NavigationBar";
import TableComponent from "./component/Table";
import MyForm from "./component/form";
import "./css/style.css";
import React, { Component } from "react";

export default class App extends Component {
  constructor() {
    super();
    this.state = {
      data: [],
    };
    this.saveData = this.saveData.bind(this);
  }
  saveData = (data) => {
    let temp = this.state.data;
    temp.push(data);
    this.setState({
      data: temp,
    });
  };
  render() {
    return (
      <div>
        <NavigationBar />
        <div className="container">
          <div className="content-left">
            <MyForm saveData={this.saveData} />
          </div>
          <div className="content-right">
            <TableComponent data={this.state.data} />
          </div>
        </div>
      </div>
    );
  }
}
